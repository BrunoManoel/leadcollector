package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead salvarLead(CadastroDeLeadDTO cadastroDeLeadDTO){
        Lead lead = cadastroDeLeadDTO.converterParaLead();

        //Preencher a data
        lead.setDataDeCadastro(LocalDate.now());

        // Separando os ids para consulta dos Produtos no Banco de Dados
        List<Integer> idDeProdutos = new ArrayList<>();
        for(IdProdutoDTO idProdutoDTO : cadastroDeLeadDTO.getProdutos()){
            int id = idProdutoDTO.getId();
            idDeProdutos.add(id);
        }
        // Pesquisa os produtos baseado no id
        Iterable<Produto> produtos = produtoRepository.findAllById(idDeProdutos);

        lead.setProdutos((List<Produto>) produtos);

        Lead objetoLead = leadRepository.save(lead);
        return objetoLead;
    }

    public List<ResumoDeLeadDTO> lerTodosOsLeads(){
        List<ResumoDeLeadDTO> listaLeadDTO = new ArrayList<>();
        for (Lead lead : leadRepository.findAll()){
            ResumoDeLeadDTO resumoDeLeadDTO = new ResumoDeLeadDTO();
            resumoDeLeadDTO.setEmail(lead.getEmail());
            resumoDeLeadDTO.setNome(lead.getNome());
            resumoDeLeadDTO.setId(lead.getId());

            listaLeadDTO.add(resumoDeLeadDTO);
        }

        return listaLeadDTO;
    }

    public Lead buscarLeadPeloId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);

        if(leadOptional.isPresent()){
            Lead lead = leadOptional.get();
            return lead;
        }else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

    public Lead atualizarLead(int id, Lead lead){
        Lead leadDB = buscarLeadPeloId(id);

        lead.setId(leadDB.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead(int id){
        if(leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        }else {
            throw new RuntimeException("Registro não existe");
        }
    }

    public Lead pesquisarPorCPF(String cpf){
        Lead lead = leadRepository.findFirstByCpf(cpf);
        if(lead != null){
            return lead;
        }else {
            throw new RuntimeException("CPF não cadastrado");
        }
    }

    public List<Lead> pesquisarPeloIdDeProduto(int id){
        return leadRepository.findByProdutosId(id);
    }
}