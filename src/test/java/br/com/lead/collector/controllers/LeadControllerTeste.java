package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    private void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Marvin");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("(42) 99876-9763");
        lead.setCpf("415.669.740-11");
        lead.setEmail("marvin@dontpanic.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setNome("Guia do Mochileiro da Galaxia");
        produto.setDescricao("um guia");

        this.produtos = Arrays.asList(produto);

        lead.setProdutos(produtos);

        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("8794821739");
        cadastroDeLeadDTO.setNome("Xxablau");
        cadastroDeLeadDTO.setEmail("email@email.com");
        cadastroDeLeadDTO.setCpf("415.329.110-25");

        List<IdProdutoDTO> idProdutoDTOS = new ArrayList<>();

        cadastroDeLeadDTO.setProdutos(idProdutoDTOS);

    }

    @Test
    public void testarBuscarPorID() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarBuscarPorIdQueNaoExiste() throws Exception{
        Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/" + lead.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarLerTodosOsLeads() throws Exception {
        List<ResumoDeLeadDTO> resumoDeLeadDTOS = new ArrayList<>();
        Mockito.when(leadService.lerTodosOsLeads()).thenReturn(resumoDeLeadDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarLead() throws Exception{
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);
        ObjectMapper objectMapper = new ObjectMapper();

        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Marvin")));

    }

    @Test
    public void testarValidacaoDeCadastroDeLead() throws Exception{
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);

        cadastroDeLeadDTO.setEmail("idygufy");
        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        Mockito.verify(leadService, Mockito.times(0))
                .salvarLead(Mockito.any(CadastroDeLeadDTO.class));
    }
}