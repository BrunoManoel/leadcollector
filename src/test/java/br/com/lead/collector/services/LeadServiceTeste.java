package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.swing.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    private void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Marvin");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("(42) 99876-9763");
        lead.setCpf("415.669.740-11");
        lead.setEmail("marvin@dontpanic.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setNome("Guia do Mochileiro da Galaxia");
        produto.setDescricao("um guia");

        this.produtos = Arrays.asList(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscaDeLeadPeloID(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadObjeto = leadService.buscarLeadPeloId(1974);

        Assertions.assertEquals(lead, leadObjeto );
        Assertions.assertEquals(lead.getDataDeCadastro(), leadObjeto.getDataDeCadastro());
    }

    @Test
    public void testarBuscaDeLeadPeloIDQueNaoExiste(){
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(7634);});
    }

    @Test
    public void testarSalvarLead(){
        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setCpf("612.812.170-50");
        cadastroDeLeadDTO.setEmail("xablau@gmail.com");
        cadastroDeLeadDTO.setNome("Jeff");
        cadastroDeLeadDTO.setTelefone("16235192");
        IdProdutoDTO idProdutoDTO = new IdProdutoDTO();
        idProdutoDTO.setId(1);
        List<IdProdutoDTO> idDeProdutos = Arrays.asList(idProdutoDTO);
        cadastroDeLeadDTO.setProdutos(idDeProdutos);

        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(this.produtos);

        // Sequestra a chamada do methodo save e retorno o proprio parametro recebido no metodo save
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0));

        Lead testeDeLead = leadService.salvarLead(cadastroDeLeadDTO);

        Assertions.assertEquals("Jeff", testeDeLead.getNome());
        Assertions.assertEquals(LocalDate.now(), testeDeLead.getDataDeCadastro());

    }
}